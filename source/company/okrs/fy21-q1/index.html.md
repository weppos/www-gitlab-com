---
layout: markdown_page
title: "FY21-Q1 OKRs"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2020 to April 30, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO Objective: IACV
* CEO KR: [Pipeline](/handbook/marketing/revenue-marketing/#revenue-marketing-kpi-definitions) is 10% ahead of plan.
* CEO KR: Sales capacity is 5% ahead of plan
* CEO KR: Renewal process satisfaction, as measured by [SSAT](/handbook/business-ops/data-team/kpi-index/#satisfaction) for tickets about renewals, is greater than 95%.

### 2. CEO: Popular next generation product
* CEO KR: [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) is being measured and reported in 100% of Sales and Product [Key Meetings](/handbook/finance/key-meetings/).
* CEO KR: [SPU](/handbook/product/metrics/#stages-per-user) increases by 0.5 stages from EOQ4 to EOQ1.
* CEO KR: [MAU](/handbook/product/metrics/#monthly-active-users-mau) increases 5% from EOQ4 to EOQ1.


### 3. CEO: Great team
* CEO KR: Teams are working [handbook-first](/handbook/handbook-usage/#why-handbook-first) and no content is being produced in other tools, e.g. Docs, Classroom, etc.
* CEO KR: There 7 certifications online for all community members to pursue. 
* CEO KR: There are 50 sessions of training discussion groups, with a focus of building out [managers](/handbook/people-group/learning-and-development/manager-development/) over the course of the quarter. 

## How to Achieve Presentations
