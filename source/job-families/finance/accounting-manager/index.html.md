---
layout: job_family_page
title: "Accounting Manager"
---

GitLab is looking for an essential member who can help drive operational improvements within our finance department. You will join our team in its early stages responsible for developing a highly efficient, world class accounting process. We expect you will know your way around GAAP principles, financial statements, and be a proven problem solver.

## Responsibilities

* Assist accounting staff on daily tasks, as needed.
* Provide training to new and existing staff, as needed.
* Ensure an accurate and timely month end close by providing leadership and support to the accounting close process. This role includes serving as a hands-on team member responsible for performing and reviewing transactions which includes but not limited to equity, intercompany and allocations.
* Assist with daily banking requirements.  Monitor and forecast cash position
* Assist in implementation of new procedures to enhance the workflow of the department.
* Enforce proper accounting policies and principles.
* Responds to inquiries from the CFO, Controller, and company wide managers regarding financial results, special reporting requests and the like.
* Assist in preparing for annual external audits.  Coordinate with other auditors as needed.
* Support overall department goals and objectives

## Requirements

* Proven work experience as an Accounting Manager or similar leadership role.
* Public company accounting experience is required
* Ability to contribute to the career development of staff and a culture of teamwork
* Strong working knowledge of GAAP principles and financial statements
* Must have experience with Netsuite
* Proficient with excel and google sheets
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* Proficiency with GitLab
* You share our [values](/handbook/values), and work in accordance with those values.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)
* Ability to use GitLab

## Senior

Senior accounting managers share the same requirements as the Intermediate Accounting Manager listed above, but also carry the following:

* Manage 3rd party accounting consultants and other accounting professional contractors as needed.
* Administrator of NetSuite including chart of accounts maintenance.
* Establish and enforce proper accounting policies and principles.
* Reviews and posts journals from the GL Accountant and/or Sr. Accountant roles.
* 1st level reviewer to various balance sheet reconciliations prepared by the GL Accountant and/or Sr. Accountant roles.
* Backup to the Senior Accounting Operations Manager

## Leader/Manager(of people) share the same requiments as the Senior Accounting Manager listed above, but also carry the following:

* Manage 1 or more people

## Performance Indicators

- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#performance-measures-for-accounting-and-finance-function-related-to-audit)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
